-- the opinions of the writer are their own and do not represent policy of Digital Vision Solutions LLC. or Norcivilian Labs. --

In my previous life I earned six times more than I do now, being a waiter is profitable. All those money were spent on my calling, art. Then, I had hundreds of thousands of listeners in total. Everyone has already forgotten, even though my friends say they remember. Then I ended up on another continent, after many trials found way back home and now I'm at Norcivilian Labs. It is a community of people who write open source programs and offer training for any related profession.

In February I worked on evenor, or event editor - it's an app to store notes and media files that runs on some complicated open and transparent technology that I don't understand. I worked on product positioning - searched for competitors, organized documentation and posed questions to narrow down the target audiences. I helped compose an application to NLNet, and some of my writing is still on the team website.

Then I enrolled in the retina project, which is an app that talks to a neural network service and provides medical reports for eyes. Basically, this is necessary for doctors who do not have the equipment to perform diagnostic tests, they can immediately upload the photo from their phone or computer and voila, they have a ready-made medical report. Then they can store the diagnostic history of their patients and see the worsening or the improvement and can help those who don't need to stand in lines or look for more expensive help elsewhere. it’s simple, there’s an attachment and that’s it. 

Well, there was also this same thing, competitor market analysis, market research, did some kind of client portrait.
At first I thought who else would do this, we are probably the only ones on the market, after analysis it turned out that just was not the case. We had competitive advantages however - we analyze diabetic retinopathy, then we have a spectrum of retinal diseases.
it wasn’t clear why the hell it was necessary to nl but then an argument was found in favor of the fact that the DVS gives access to the neural network and went, "good, well done", - the guys get experience working with neural networks and coding applications and, in principle, have the experience that you created an application from scratch for Apple and released everything fuck well done you are accepted.

Next were the user stories. I gained access to the test version of the application, compared it and what the stakeholder already had, that is, this is the website and their presentation of how they see the application. Based on this data, from this analysis, I wrote the requirements. Then I put myself in the user’s shoes, assessed what features were needed in the application, and if this was not in the stakeholder’s version or in ours, I simply wrote it in as additional ideas.
For example, I used a test application from Android and in order to return I had to re-enter the application every time and I formulated that the user should have a back button. 
I closely studied the stakeholder’s presentation to analyze what he himself sees in this project and in the final release of the application, from that I made requirements, 17 pieces. From the requirements I could now build a roadmap. 

In general, at first I studied a bunch of articles on how to make this roadmap. There were just dudes telling cases of how these roadmaps were compiled in certain projects. usually based on asking the right questions to the stakeholder. I made up questions for the stakeholder. even just compiling this list of questions had a fruitful influence on the creation of the roadmap. based on all this I made a roadmap.

All work in NL is set up in GitLab and the communication happens in public at Zulip. You probably wonder how I learned it all. It was hard, it wasn’t clear to me what was this GitLab initially, but naturally, like a fucking zoomer, I looked at tutorials on how to use GitLab, studied the most popular features, asked the mentor, - why a merge request, what is it, asked a bunch of questions to figure it out. I created a GitLab Milestone and at that point I realized, that’s it, it’s clear now.
Milestone works similar to labels. Labels are not crap, I divided issues into "core" and "idea". I recommend using them, it doesn’t require effort and simplifies the work process for everyone. 
When creating issues from the requirements, I inserted all the necessary screenshots into the issues so that the engineers could understand what they need to do. I searched for the icons in the team cloud, saved them, and passed them on to engineers. I also provided feedback on blockers and estimated deadlines for future tasks.

With the roadmap composed, I needed to set deadlines and built a gantt chart. 
Setting correct deadlines is a long process. At first I just roughly estimated the timing after preliminary communication with the engineers. Then, after a few weeks, I looked at how much time was actually spent on tasks, moved the deadlines and then I practically didn’t have to move it. Just recently it was the holidays and I remembered to account for them, but for the last month everything was going smoothly.

Building the gantt chart goes through several stages. We use ganttlab and it is formed on the basis of the issues and due dates that I have assigned for each task. With the chart ready, I sent to the stakeholder and all engineers.

In truth, right now there is already a completely different gantt than it was at the beginning, initially, when I first arrived. Here, I saved several variations of this gantt. After some time it became clear how much time actually was spent and the gantt was changed to reflect reality.

[add all the screenshots of how the gantt changed]

Then I worked on my resume following NL's best practices, but that's a different story, and instead I've thought I'd share some advice that I myself could use several months ago.

First of all, I recommend communication with a mentor to everyone. The skill to ask open questions was helpful. That is, everything was unexpected for me. When I was studying this application, everything was very complicated and it was unclear what was what, what function was needed for what, who is the user, who will use, what is this for in general? I asked a bunch of clarifying questions and then everything became clear. 
Also, i was glad I stored all brainstorms, questions and answers in the cloud. 

So far there are refusals for all product vacancies I've applied to, and when I read the job descriptions, what I still don't know how to do catches my eye, I notice the gaps and look forward to closing them. Thanks to folks at Norcivilian Labs I have plenty opportunities for that.

