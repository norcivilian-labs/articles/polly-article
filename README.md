-- Мнения автора являются их собственными и не отражают политику компании Digital Vision Solutions LLC. или Norcivilian Labs. --

За последние 10 лет я сменила с десяток разных профессий и мест работы. По большей части, я работала в сфере услуг. Параллельно я руководила своим музыкальным проектом и все, что зарабатывала - тратила на его развитие. Тогда у меня были в общей сложности сотни тысяч прослушиваний. Затем я оказалась на другом континенте, после многих испытаний вернулась домой, попробовала себя сначала в знакомых сферах, и спустя какое-то время я оказалась в лабораториях Norcivilian. Это сообщество людей, которые пишут программы с открытым исходным кодом и предлагают обучение любой смежной профессии.

В феврале я работала над Evenor или редактором событий — приложением для хранения заметок и медиа-файлов, которое работает на сложной открытой и прозрачной технологии, которую изначально без какого-либо технического бекграунда я не понимала. Работала над позиционированием продукта — искала конкурентов, организовывала документацию и ставила вопросы, чтобы сузить целевую аудиторию. Я помогала составлять заявку в NLNet, и кое-что из того, что я написала, до сих пор находится на веб-сайте команды.

Затем я зарегистрировалась в проекте Retina — приложении, которое взаимодействует со службой нейронной сети и предоставляет медицинские заключения о состоянии глаз. В основном это нужно врачам, у которых нет оборудования для проведения диагностических исследований, они могут сразу загрузить фото с телефона или компьютера и вуаля, у них готовое медицинское заключение. Затем они смогут хранить историю диагностики своих пациентов и видеть ухудшение или улучшение, а также помогать тем, у кого нет доступа к необходимым медицинским учреждениям, кому не нужно стоять в очередях или искать более дорогую помощь в другом месте. Все просто, есть приложение и все.

Также я провела анализ рынка конкурентов, создав портрет клиента.
Сначала я думала, что аналогов такого приложения нет, и мы, наверное, единственные на рынке. После исследования выяснилось, что несколько подобных продуктов есть. Однако у нас были конкурентные преимущества — мы анализируем не только диабетическую ретинопатию (как все наши конкуренты), но у нас есть целый спектр заболеваний сетчатки.
У меня стоял вопрос, зачем вообще этот проект NL, но потом нашелся аргумент в пользу того, что стейкхолдер дает доступ к нейросети. Ребята получают опыт работы с нейронкой и приложения для кодирования, а также опыт создания с нуля приложения для Apple.

Дальше были пользовательские истории. Я получила доступ к тестовой версии приложения, сравнила ее и то, что уже было у стейкхолдера, то есть это сайт и их презентация того, как они видят приложение. На основе этих данных, из этого анализа я написала требования. Дальше я ставила себя на место пользователя, оценивала, какие функции нужны в приложении, и если этого не было ни в версии стейкхолдера, ни в нашей, я просто вписывала это в качестве дополнительных идей.
Например, я использовала тестовое приложение от Android и чтобы вернуться, мне приходилось каждый раз заново заходить в приложение и я сформулировала, что у пользователя должна быть кнопка назад.
Я внимательно изучила презентацию стейкхолдера, чтобы проанализировать, что он сам видит в этом проекте и в финальном релизе приложения, из чего составила требования, 17 штук. Из требований я теперь могла построить дорожную карту.

Сначала я изучила кучу статей о том, как вообще составить эту дорожную карту. Просто разные продакты рассказывали случаи, как эти дорожные карты составлялись в тех или иных проектах. Обычно основано на задании правильных вопросов заинтересованной стороне. Я составила список вопросов. Даже простое составление этого списка вопросов оказало плодотворное влияние на создание дорожной карты. на основе всего этого я составила ее.

Вся работа в NL ведется в GitLab, а общение происходит публично в Zulip. Вы, наверное, задаетесь вопросом, как я всему этому научилась. Это было тяжело, мне изначально был непонятен функционал GitLab, но естественно, как охрененный зумер, я смотрела туториалы по использованию GitLab, изучала самые популярные фичи, спрашивала у наставника, - зачем мерж-реквест , что это такое, задавала кучу вопросов, чтобы разобраться. Я создала GitLab Milestone и проставила лейблы. Я разделила вопросы на "ядро" и "идею". Рекомендую их использовать, это не требует усилий и упрощает процесс работы каждому.
При создании задач из требований я вставляла в задачи все необходимые скриншоты, чтобы инженеры могли понять, что им нужно делать. Я искала значки в облаке команды, сохраняла их и передала инженерам. Также я предоставила отзывы о блокерах и примерные сроки выполнения будущих задач.

Когда дорожная карта была составлена, мне нужно было установить сроки и построить диаграмму Ганта.
Установление правильных сроков — длительный процесс. Сначала я просто примерно оценила cроки после предварительного общения с инженерами. Потом, через несколько недель, я оценила, сколько времени на самом деле уходит на задачи, сдвинула сроки и тогда мне практически не пришлось их сдвигать. 

Построение диаграммы Ганта проходит в несколько этапов. Мы используем ganttlab и он формируется на основе задач и сроков выполнения, которые я назначила для каждой задачи. Готовую схему я отправила заинтересованным сторонам и всем инженерам.По правде говоря, сейчас там уже совсем другой Гантт, чем была вначале, изначально, когда я только приехал. Здесь я сохранила несколько вариантов этого гантта. Через некоторое время стало ясно, сколько времени на самом деле было потрачено, и диаграмма была изменена, чтобы отразить реальность.

[добавьте все скриншоты того, как менялся Гантт]

Затем я работала над своим резюме, следуя лучшим практикам NL, но это уже другая история, и вместо этого я решила поделиться некоторыми советами, которые мне самой могли бы пригодиться несколько месяцев назад.

Прежде всего, я всем рекомендую общение с наставником. Умение задавать открытые вопросы было полезным. То есть для меня все было неожиданно. Когда я изучала это приложение, все было очень сложно и было непонятно, что к чему, какая функция для чего нужна, кто пользователь, кто будет пользоваться, для чего это вообще? Я задала кучу уточняющих вопросов и тогда все стало понятно.
Кроме того, я была рада, что сохранила все мозговые штурмы, вопросы и ответы в облаке.

Пока на все продуктовые вакансии, на которые я подавала заявки, я получаю отказы, и когда я читаю описания вакансий, в глаза бросается то, что я еще не умею делать, замечаю пробелы и жду возможности их закрыть. Благодаря ребятам из Norcivilian Labs у меня для этого много возможностей.
